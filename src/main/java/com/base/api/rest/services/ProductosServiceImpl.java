package com.base.api.rest.services;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.api.rest.entity.Productos;
import com.base.api.rest.respository.ProductosRepository;
@Service
public class ProductosServiceImpl implements ProductosService {

	@Autowired
	private ProductosRepository productoRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Productos> findAll() {
		return (List<Productos>) productoRepository.findAll();
	}

	
}
