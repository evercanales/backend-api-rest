package com.base.api.rest.services;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.base.api.rest.entity.Usuarios;
import com.base.api.rest.respository.UsuarioRepository;

@Service
public class UsuarioService implements UserDetailsService{
	
	private Logger logger= LoggerFactory.getLogger(UsuarioService.class);
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException {

		Usuarios user =usuarioRepository.findByUsuario(usuario);
		
		if(user==null) {
			logger.error("Error en el login: no existe el usuario "+usuario+" en el sistema!");
			throw new UsernameNotFoundException("Error en el login: no existe el usuario "+usuario+" en el sistema!");
		}
		
		List<GrantedAuthority> authorities = user.getRolesList().stream()
				.map(role -> new SimpleGrantedAuthority(role.getRol()))
				.peek(authoririty -> logger.info("Role: "+authoririty.getAuthority() ))
				.collect(Collectors.toList());
		
		return new User(user.getUsuario(), user.getContrasena(), (user.getActivo().compareTo('A') == 0), true, true, true, authorities);
		}

}
