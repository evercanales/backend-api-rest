package com.base.api.rest.services;


import java.util.List;
import com.base.api.rest.entity.Productos;

public interface ProductosService {

	public List<Productos> findAll();

}
