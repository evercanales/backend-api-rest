package com.base.api.rest.dto;

public class ProductoResponseDTO {

private Long idProducto ;
private String numeroRegistro;
private String descripcion;

public Long getIdProducto() {
	return idProducto;
}
public void setIdProducto(Long idProducto) {
	this.idProducto = idProducto;
}

public String getNumeroRegistro() {
	return numeroRegistro;
}
public void setNumeroRegistro(String numeroRegistro) {
	this.numeroRegistro = numeroRegistro;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
	
}
