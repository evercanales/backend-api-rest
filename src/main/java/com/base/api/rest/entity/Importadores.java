package com.base.api.rest.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author er2
 */
@Entity
@Table(name = "IMPORTADORES")
public class Importadores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generadorImportador")
    @SequenceGenerator(name = "generadorImportador", sequenceName = "S_IMPORTADORES", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_IMPORTADOR")
    private Long idImportador;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 255)
    @Column(name = "NIT")
    private String nit;
    @Size(max = 255)
    @Column(name = "NOMBRE_COMERCIAL")
    private String nombreComercial;
    @Size(max = 255)
    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;
    @Size(max = 255)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Size(max = 255)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID_ESTADO")
    @ManyToOne
    private Estados idEstado;

    public Importadores() {
    }

    public Importadores(Long idImportador) {
        this.idImportador = idImportador;
    }

    public Long getIdImportador() {
        return idImportador;
    }

    public void setIdImportador(Long idImportador) {
        this.idImportador = idImportador;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idImportador != null ? idImportador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Importadores)) {
            return false;
        }
        Importadores other = (Importadores) object;
        if ((this.idImportador == null && other.idImportador != null) || (this.idImportador != null && !this.idImportador.equals(other.idImportador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.base.sbasejsf.pojos.Importadores[ idImportador=" + idImportador + " ]";
    }
    
}
