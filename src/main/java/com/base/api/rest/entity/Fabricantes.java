package com.base.api.rest.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author er2
 */
@Entity
@Table(name = "FABRICANTES")
@NamedQueries({
    @NamedQuery(name = "Fabricantes.findAll", query = "SELECT f FROM Fabricantes f"),
    @NamedQuery(name = "Fabricantes.findByIdFabricante", query = "SELECT f FROM Fabricantes f WHERE f.idFabricante = :idFabricante"),
    @NamedQuery(name = "Fabricantes.findByFechaCreacion", query = "SELECT f FROM Fabricantes f WHERE f.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Fabricantes.findByFechaModificacion", query = "SELECT f FROM Fabricantes f WHERE f.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "Fabricantes.findByNombre", query = "SELECT f FROM Fabricantes f WHERE f.nombre = :nombre"),
    @NamedQuery(name = "Fabricantes.findByUsuarioCreacion", query = "SELECT f FROM Fabricantes f WHERE f.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "Fabricantes.findByUsuarioModificacion", query = "SELECT f FROM Fabricantes f WHERE f.usuarioModificacion = :usuarioModificacion")})
public class Fabricantes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generadorFabricante")
    @SequenceGenerator(name = "generadorFabricante", sequenceName = "S_FABRICANTES", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FABRICANTE")
    private Long idFabricante;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 255)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 255)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Size(max = 255)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @OneToMany(mappedBy = "idFabricante")
    private List<Productos> productosList;

    public Fabricantes() {
    }

    public Fabricantes(Long idFabricante) {
        this.idFabricante = idFabricante;
    }

    public Long getIdFabricante() {
        return idFabricante;
    }

    public void setIdFabricante(Long idFabricante) {
        this.idFabricante = idFabricante;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public List<Productos> getProductosList() {
        return productosList;
    }

    public void setProductosList(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFabricante != null ? idFabricante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fabricantes)) {
            return false;
        }
        Fabricantes other = (Fabricantes) object;
        if ((this.idFabricante == null && other.idFabricante != null) || (this.idFabricante != null && !this.idFabricante.equals(other.idFabricante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.base.sbasejsf.pojos.Fabricantes[ idFabricante=" + idFabricante + " ]";
    }
    
}
