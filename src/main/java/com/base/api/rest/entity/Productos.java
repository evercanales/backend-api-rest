package com.base.api.rest.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author er2
 */
@Entity
@Table(name = "PRODUCTOS")
@NamedQueries({
    @NamedQuery(name = "Productos.findAll", query = "SELECT p FROM Productos p"),
    @NamedQuery(name = "Productos.findByIdProducto", query = "SELECT p FROM Productos p WHERE p.idProducto = :idProducto"),
    @NamedQuery(name = "Productos.findByFechaCreacion", query = "SELECT p FROM Productos p WHERE p.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Productos.findByFechaModificacion", query = "SELECT p FROM Productos p WHERE p.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "Productos.findByNombreComercial", query = "SELECT p FROM Productos p WHERE p.nombreComercial = :nombreComercial"),
    @NamedQuery(name = "Productos.findByNombreProducto", query = "SELECT p FROM Productos p WHERE p.nombreProducto = :nombreProducto"),
    @NamedQuery(name = "Productos.findByNumeroRegistro", query = "SELECT p FROM Productos p WHERE p.numeroRegistro = :numeroRegistro"),
    @NamedQuery(name = "Productos.findByUsuarioCreacion", query = "SELECT p FROM Productos p WHERE p.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "Productos.findByUsuarioModificacion", query = "SELECT p FROM Productos p WHERE p.usuarioModificacion = :usuarioModificacion"),
    @NamedQuery(name = "Productos.findByVigenteHasta", query = "SELECT p FROM Productos p WHERE p.vigenteHasta = :vigenteHasta")})
public class Productos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generadorProducto")
    @SequenceGenerator(name = "generadorProducto", sequenceName = "S_Productos", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PRODUCTO")
    private Long idProducto;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 255)
    @Column(name = "NOMBRE_COMERCIAL")
    private String nombreComercial;
    @Size(max = 255)
    @Column(name = "NOMBRE_PRODUCTO")
    private String nombreProducto;
    @Size(max = 255)
    @Column(name = "NUMERO_REGISTRO")
    private String numeroRegistro;
    @Size(max = 255)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Size(max = 255)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @Column(name = "VIGENTE_HASTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vigenteHasta;
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID_ESTADO")
    @ManyToOne
    private Estados idEstado;
    @JoinColumn(name = "ID_FABRICANTE", referencedColumnName = "ID_FABRICANTE")
    @ManyToOne
    private Fabricantes idFabricante;

    public Productos() {
    }

    public Productos(Long idProducto) {
        this.idProducto = idProducto;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getNumeroRegistro() {
        return numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public Date getVigenteHasta() {
        return vigenteHasta;
    }

    public void setVigenteHasta(Date vigenteHasta) {
        this.vigenteHasta = vigenteHasta;
    }

    public Estados getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    public Fabricantes getIdFabricante() {
        return idFabricante;
    }

    public void setIdFabricante(Fabricantes idFabricante) {
        this.idFabricante = idFabricante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productos)) {
            return false;
        }
        Productos other = (Productos) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.base.sbasejsf.pojos.Productos[ idProducto=" + idProducto + " ]";
    }
    
}
