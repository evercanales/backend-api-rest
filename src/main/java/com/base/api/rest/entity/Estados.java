package com.base.api.rest.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author er2
 */
@Entity
@Table(name = "ESTADOS")
@NamedQueries({
    @NamedQuery(name = "Estados.findAll", query = "SELECT e FROM Estados e"),
    @NamedQuery(name = "Estados.findByIdEstado", query = "SELECT e FROM Estados e WHERE e.idEstado = :idEstado"),
    @NamedQuery(name = "Estados.findByFechaCreacion", query = "SELECT e FROM Estados e WHERE e.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "Estados.findByFechaModificacion", query = "SELECT e FROM Estados e WHERE e.fechaModificacion = :fechaModificacion"),
    @NamedQuery(name = "Estados.findByNombreEstado", query = "SELECT e FROM Estados e WHERE e.nombreEstado = :nombreEstado"),
    @NamedQuery(name = "Estados.findByUsuarioCreacion", query = "SELECT e FROM Estados e WHERE e.usuarioCreacion = :usuarioCreacion"),
    @NamedQuery(name = "Estados.findByUsuarioModificacion", query = "SELECT e FROM Estados e WHERE e.usuarioModificacion = :usuarioModificacion")})
public class Estados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generadorEstado")
    @SequenceGenerator(name = "generadorEstado", sequenceName = "S_ESTADOS", allocationSize = 1)
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTADO")
    private Long idEstado;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Size(max = 255)
    @Column(name = "NOMBRE_ESTADO")
    private String nombreEstado;
    @Size(max = 255)
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @Size(max = 255)
    @Column(name = "USUARIO_MODIFICACION")
    private String usuarioModificacion;
    @OneToMany(mappedBy = "idEstado")
    private List<Importadores> importadoresList;
    @OneToMany(mappedBy = "idEstado")
    private List<Productos> productosList;

    public Estados() {
    }

    public Estados(Long idEstado) {
        this.idEstado = idEstado;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }

    public List<Importadores> getImportadoresList() {
        return importadoresList;
    }

    public void setImportadoresList(List<Importadores> importadoresList) {
        this.importadoresList = importadoresList;
    }

    public List<Productos> getProductosList() {
        return productosList;
    }

    public void setProductosList(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstado != null ? idEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estados)) {
            return false;
        }
        Estados other = (Estados) object;
        if ((this.idEstado == null && other.idEstado != null) || (this.idEstado != null && !this.idEstado.equals(other.idEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + nombreEstado + "";
    }
    
}
