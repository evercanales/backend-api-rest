package com.base.api.rest.respository;

import org.springframework.data.repository.CrudRepository;

import com.base.api.rest.entity.Usuarios;

public interface UsuarioRepository extends CrudRepository<Usuarios, Long>{
	
	Usuarios findByUsuario(String usuario);

}
