package com.base.api.rest.respository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.base.api.rest.entity.Productos;

public interface ProductosRepository extends JpaRepository<Productos, Long>{

	@Query("from Productos")
	public List<Productos> findAllProductos();
}
