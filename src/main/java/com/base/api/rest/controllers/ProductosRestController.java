package com.base.api.rest.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.base.api.rest.dto.ProductoResponseDTO;
import com.base.api.rest.entity.Productos;
import com.base.api.rest.services.ProductosService;


@RestController
@RequestMapping("/api")
public class ProductosRestController {

	@Autowired
	private ProductosService productosService;
	
	
	@GetMapping("/productos")
	public List<ProductoResponseDTO> index() {
		List<Productos> productosList=productosService.findAll();
		List<ProductoResponseDTO> productos =
				productosList.stream()
			                .map(p -> {
			                	ProductoResponseDTO productoDTO=new ProductoResponseDTO();
			                	productoDTO.setDescripcion(p.getNombreProducto());
			                	productoDTO.setIdProducto(p.getIdProducto());
			                	productoDTO.setNumeroRegistro(p.getNumeroRegistro());
			                    return productoDTO;
			                })
			                .collect(Collectors.toList());
		return productos;
	}
	
	}
